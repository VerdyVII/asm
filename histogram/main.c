#include <err.h>
#include <stdlib.h>
#include <stdio.h>

int *histogram(char *s, int *arr);

int main(int argc, char **argv)
{
    if (argc != 2)
        errx(1, "Usage: %s <string>", argv[0]);

    int *arr = calloc(26, sizeof(int));

    histogram(argv[1], arr);
    for (int i = 0; i < 26; i++)
        printf("%c: %d\n", 'a' + i, arr[i]);
    free(arr);
    return 0;
}
